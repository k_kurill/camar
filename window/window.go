package window

import (
	"github.com/go-gl/gl/v3.2-core/gl"
	"github.com/go-gl/glfw/v3.2/glfw"
	"github.com/golang-ui/nuklear/nk"
)

type Win struct {
	*glfw.Window
	InputManager *InputManager
}

func Init(width int, height int, title string) *glfw.Window {
	if err := gl.Init(); err != nil {
		panic(err)
	}
	if err := glfw.Init(); err != nil {
		panic(err)
	}

	glfw.WindowHint(glfw.ContextVersionMajor, 3) // OR 2
	glfw.WindowHint(glfw.ContextVersionMinor, 2)
	glfw.WindowHint(glfw.OpenGLProfile, glfw.OpenGLCoreProfile)
	glfw.WindowHint(glfw.OpenGLForwardCompatible, glfw.True)
	glfw.WindowHint(glfw.Resizable, glfw.True)
	glfw.WindowHint(glfw.OpenGLDebugContext, 1)
	glfw.WindowHint(glfw.Samples, 4)

	win, _ := glfw.CreateWindow(width, height, title, nil, nil)

	//monitor := glfw.GetPrimaryMonitor()
	//mode := monitor.GetVideoMode()
	//window, err := glfw.CreateWindow(int(mode.Width/2), int(mode.Height/2), title, nil, nil)

	win.MakeContextCurrent()
	win.SetCloseCallback(onClose)
	return win
}

func (win *Win) SetCallbacks() {
	win.SetKeyCallback(win.InputManager.keyCallback)
	win.SetCursorPosCallback(win.InputManager.mouseCallback)
}

func onClose(w *glfw.Window) {
	nk.NkPlatformShutdown()
	w.Destroy()
}
