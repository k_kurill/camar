package window

import (
	"github.com/go-gl/glfw/v3.2/glfw"
	"github.com/go-gl/mathgl/mgl64"
)

// Action is a configurable abstraction of a key press
type Action int

const (
	PlayerForward  Action = iota
	PlayerBackward Action = iota
	PlayerLeft     Action = iota
	PlayerRight    Action = iota
	ProgramQuit    Action = iota
	Scene0         Action = iota
	Scene1         Action = iota
	Scene2         Action = iota
	Scene3         Action = iota
	Scene4         Action = iota
	Scene5         Action = iota
	Scene6         Action = iota
	Scene7         Action = iota
	Scene8         Action = iota
	Scene9         Action = iota
)

type InputManager struct {
	actionToKeyMap map[Action]glfw.Key
	keysPressed    [glfw.KeyLast]bool

	firstCursorAction    bool
	lockCursor           bool
	cursor               mgl64.Vec2
	cursorChange         mgl64.Vec2
	cursorLast           mgl64.Vec2
	bufferedCursorChange mgl64.Vec2
}

func NewInputManager() *InputManager {
	actionToKeyMap := map[Action]glfw.Key{
		PlayerForward:  glfw.KeyW,
		PlayerBackward: glfw.KeyS,
		PlayerLeft:     glfw.KeyA,
		PlayerRight:    glfw.KeyD,
		ProgramQuit:    glfw.KeyEscape,
		Scene0:         glfw.Key0,
		Scene1:         glfw.Key1,
		Scene2:         glfw.Key2,
		Scene3:         glfw.Key3,
		Scene4:         glfw.Key4,
		Scene5:         glfw.Key5,
		Scene6:         glfw.Key6,
		Scene7:         glfw.Key7,
		Scene8:         glfw.Key8,
		Scene9:         glfw.Key9,
	}

	return &InputManager{
		actionToKeyMap:    actionToKeyMap,
		firstCursorAction: true,
		lockCursor:        true,
	}
}

// IsActive returns whether the given Action is currently active
func (im *InputManager) IsActive(a Action) bool {
	return im.keysPressed[im.actionToKeyMap[a]]
}

// Cursor returns the value of the cursor at the last time that CheckpointCursorChange() was called.
func (im *InputManager) Cursor() mgl64.Vec2 {
	return im.cursor
}

// CursorChange returns the amount of change in the underlying cursor
// since the last time CheckpointCursorChange was called
func (im *InputManager) CursorChange() mgl64.Vec2 {
	return im.cursorChange
}

// CheckpointCursorChange updates the publicly available Cursor() and CursorChange()
// methods to return the current Cursor and change since last time this method was called.
func (im *InputManager) CheckpointCursorChange() {
	im.cursorChange[0] = im.bufferedCursorChange[0]
	im.cursorChange[1] = im.bufferedCursorChange[1]
	im.cursor[0] = im.cursorLast[0]
	im.cursor[1] = im.cursorLast[1]

	im.bufferedCursorChange[0] = 0
	im.bufferedCursorChange[1] = 0
}

func (im *InputManager) keyCallback(window *glfw.Window, key glfw.Key, scancode int,
	action glfw.Action, mods glfw.ModifierKey) {

	// timing for key events occurs differently from what the program loop requires
	// so just track what key actions occur and then access them in the program loop
	switch action {
	case glfw.Press:
		im.keysPressed[key] = true
	case glfw.Release:
		im.keysPressed[key] = false
	}
}

func (im *InputManager) mouseCallback(window *glfw.Window, xpos, ypos float64) {
	if window.GetMouseButton(glfw.MouseButtonRight) == glfw.Press {
		im.bufferedCursorChange[0] += xpos - im.cursorLast[0]
		im.bufferedCursorChange[1] += ypos - im.cursorLast[1]
		window.SetInputMode(glfw.CursorMode, glfw.CursorHidden)
		w, h := window.GetSize()
		xpos = float64(w / 2)
		ypos = float64(h / 2)
		window.SetCursorPos(xpos, ypos)
	} else {
		window.SetInputMode(glfw.CursorMode, glfw.CursorNormal)
	}

	im.cursorLast[0] = xpos
	im.cursorLast[1] = ypos

}
