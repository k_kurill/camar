package rtsp

import (
	"fmt"
	"github.com/nareix/joy4/av"
	"github.com/nareix/joy4/av/avutil"
	"github.com/nareix/joy4/format"
)

const (
	source = "rtsp://170.93.143.139/rtplive/470011e600ef003a004ee33696235daa"
)

type Rtsp struct {
	src     av.DemuxCloser
	streams []av.CodecData
}

func (r *Rtsp) Init() {
	format.RegisterAll()
	var err error
	r.src, err = avutil.Open(source)
	if err != nil {
		panic(err)
	}
	r.streams, _ = r.src.Streams()
	for _, stream := range r.streams {
		if stream.Type().IsAudio() {
			astream := stream.(av.AudioCodecData)
			fmt.Println(astream.Type(), astream.SampleRate(), astream.SampleFormat(), astream.ChannelLayout())
		} else if stream.Type().IsVideo() {
			vstream := stream.(av.VideoCodecData)
			fmt.Println(vstream.Type(), vstream.Width(), vstream.Height())
		}
	}
}

func (r *Rtsp) GetData() []byte {

	var data []byte

	for {
		var pkt av.Packet

		var err error
		if pkt, err = r.src.ReadPacket(); err != nil {
			break
		}
		if pkt.IsKeyFrame && len(data) > 0 {
			return data
		}
		data = append(data, pkt.Data...)

		fmt.Println("pkt", pkt.Idx, r.streams[pkt.Idx].Type(), "len", len(pkt.Data), "keyframe", pkt.IsKeyFrame)
	}
	return data
}
