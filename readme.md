### Установка Go
`sudo apt install golang-go`
### OpenGl GO
`github.com/go-gl/gl/v3.2-core/gl`

### GLFW
`github.com/go-gl/glfw/v3.2/glfw`

### GUI
`go get github.com/golang-ui/nuklear/nk`

### 3D Math lib
`go get github.com/go-gl/mathgl/mgl32`

### Lib to read 3D files (*.obj, *.mtl)
`go get github.com/udhos/gwob`

### Lib to read rtsp video
`go get "github.com/nareix/joy4/format/rtsp"`
