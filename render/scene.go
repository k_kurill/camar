package render

import (
	"camar/cam"
	"camar/rtsp"
	"camar/window"
	"github.com/go-gl/gl/v3.2-core/gl"
	"github.com/go-gl/mathgl/mgl32"
	"log"
	"os"
)

type Scene struct {
	*GlStateManager
	rtsp            *rtsp.Rtsp
	Name            string
	BasicShaderPath string
	LoadModel       bool
	TextureName     string
	models          []*Object
	camera          *cam.FpsCamera
}

func (scene *Scene) Load(im *window.InputManager) {
	scene.Reset()
	scene.camera = cam.NewFpsCamera(mgl32.Vec3{0, 0, 30}, mgl32.Vec3{0, 1, 0}, -90, 0, im)
	scene.loadShaders()
	scene.loadModels()
	//scene.rtsp = new(rtsp.Rtsp)
	//scene.rtsp.Init()
}

func (scene *Scene) Update() {
	//log.Println(gl.GetError())
	gl.Clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT)
	gl.ClearColor(1, 1, 1, 0.0)
	//scene.rtsp.GetData()

	cursorPosition := scene.camera.GetCursorPosition()
	log.Println(cursorPosition)
	scene.useProgram("basic")
	gl.Uniform1f(scene.programs["basic"].GetUniformLocation("u_time"), 0.)
	gl.Uniform1f(scene.programs["basic"].GetUniformLocation("u_rand"), float32(cursorPosition[0]/1000))
	gl.Uniform1f(scene.programs["basic"].GetUniformLocation("u_noise"), float32(cursorPosition[1]/1000))

	gl.Uniform1i(scene.programs["basic"].GetUniformLocation("nrows"), int32(cursorPosition[0]))
	gl.Uniform1i(scene.programs["basic"].GetUniformLocation("ncols"), int32(cursorPosition[1]))

	scene.updateCamera()
	scene.updateTimer()
	camTransform := scene.camera.GetTransform()
	scene.useProgram("basicCamera")
	gl.UniformMatrix4fv(scene.programs["basicCamera"].GetUniformLocation("camera"), 1, false, &camTransform[0])
	gl.UniformMatrix4fv(scene.programs["basicCamera"].GetUniformLocation("project"), 1, false, &scene.projectionMatrix[0])
	scene.renderModels()
}

//Update camera position and direction from input evevnts
func (scene *Scene) updateCamera() {
	scene.camera.Update(float64(scene.frameDelta))
}

func (scene *Scene) loadModels() {
	dir, _ := os.Getwd()
	fileObj := dir + "/assets/objects/screen.obj"
	obj := new(Object)
	obj.Load(fileObj)
	obj.Program = scene.programs["basic"]
	fileObj = dir + "/assets/textures/" + scene.TextureName + ".jpg"
	obj.Texture, _ = NewTextureFromFile(fileObj, gl.CLAMP_TO_EDGE, gl.CLAMP_TO_EDGE)
	scene.models = append(scene.models, obj)
	if scene.LoadModel {
		fileObj = dir + "/assets/objects/red_cube.obj"
		obj = new(Object)
		obj.Load(fileObj)
		obj.Program = scene.programs["basicCamera"]
		scene.models = append(scene.models, obj)
	}
}

func (scene *Scene) loadShaders() {
	dir, _ := os.Getwd()

	fileObj := dir + "/assets/shaders/vertex/" + scene.BasicShaderPath + "basic.glsl"
	vertexShader, err := NewShaderFromFile(fileObj, gl.VERTEX_SHADER)
	if err != nil {
		panic(err)
	}

	fileObj = dir + "/assets/shaders/fragment/" + scene.BasicShaderPath + "basic.glsl"
	fragmentShader, err := NewShaderFromFile(fileObj, gl.FRAGMENT_SHADER)
	if err != nil {
		panic(err)
	}

	scene.GlStateManager.programs["basic"] = NewProgram(vertexShader, fragmentShader)

	fileObj = dir + "/assets/shaders/vertex/basicCamera.glsl"
	vertexShader, err = NewShaderFromFile(fileObj, gl.VERTEX_SHADER)
	if err != nil {
		panic(err)
	}

	fileObj = dir + "/assets/shaders/fragment/basicCamera.glsl"
	fragmentShader, err = NewShaderFromFile(fileObj, gl.FRAGMENT_SHADER)
	if err != nil {
		panic(err)
	}

	scene.GlStateManager.programs["basicCamera"] = NewProgram(vertexShader, fragmentShader)
}

func (scene *Scene) renderModels() {
	for _, model := range scene.models {
		model.Render()
	}
}
