package render

import (
	"fmt"
	"github.com/go-gl/gl/v3.2-core/gl"
	"github.com/udhos/gwob"
	"log"
	"os"
)

type Object struct {
	Program *Program
	Texture *Texture
	obj     *gwob.Obj
	points  []float32
	vao     uint32
}

func (obj *Object) Load(fileObj string) {
	// Set options
	options := &gwob.ObjParserOptions{
		LogStats: true,
		Logger:   func(msg string) { fmt.Fprintln(os.Stderr, msg) },
	}
	// Load OBJ
	o, errObj := gwob.NewObjFromFile(fileObj, options)
	if errObj != nil {
		log.Printf("obj: parse error input=%s: %v", fileObj, errObj)
		return
	}
	obj.obj = o
	obj.makeVao()
}

func (obj *Object) Render() {
	obj.Program.Use()
	if obj.Texture != nil {
		obj.Texture.Bind(gl.TEXTURE0)
		obj.Texture.SetUniform(obj.Program.GetUniformLocation("ourTexture0"))
		gl.PolygonMode(gl.FRONT_AND_BACK, gl.FILL)
	} else {
		gl.PolygonMode(gl.FRONT_AND_BACK, gl.LINE)
	}
	gl.BindVertexArray(obj.vao)

	gl.DrawArrays(gl.TRIANGLES, 0, int32(len(obj.points)))
	//gl.DrawElements(gl.TRIANGLES,0,gl.UNSIGNED_INT,unsafe.Pointer(nil))
	gl.PolygonMode(gl.FRONT_AND_BACK, gl.FILL)
}

// makeVao initializes and returns a vertex array from the points provided.
func (obj *Object) makeVao() {
	obj.orderPoints(obj.obj)
	var vbo uint32
	gl.GenBuffers(1, &vbo)
	gl.BindBuffer(gl.ARRAY_BUFFER, vbo)
	gl.BufferData(gl.ARRAY_BUFFER, 4*len(obj.points), gl.Ptr(obj.points), gl.STATIC_DRAW)

	var vao uint32
	gl.GenVertexArrays(1, &vao)
	gl.BindVertexArray(vao)
	gl.EnableVertexAttribArray(0)
	gl.BindBuffer(gl.ARRAY_BUFFER, vbo)
	gl.VertexAttribPointer(0, 3, gl.FLOAT, false, 0, nil)

	// texture position
	gl.VertexAttribPointer(2, 2, gl.FLOAT, false, 3*4+2*4, gl.PtrOffset(3*4))
	gl.EnableVertexAttribArray(1)
	obj.vao = vao
}

func (obj *Object) orderPoints(o *gwob.Obj) []float32 {
	for _, i := range o.Indices {
		x, y, z := o.VertexCoordinates(i)
		obj.points = append(obj.points, x)
		obj.points = append(obj.points, y)
		obj.points = append(obj.points, z)
	}
	return obj.points
}
