package render

import (
	"camar/window"
	"fmt"
	"github.com/go-gl/gl/v3.2-core/gl"
	"github.com/go-gl/glfw/v3.2/glfw"
	"github.com/go-gl/mathgl/mgl32"
	"strings"
	"time"
)

type GlStateManager struct {
	Window         *window.Win
	prog           *Program            // current active shader program
	programs       map[string]*Program // shader programs cache
	viewportX      int32
	viewportY      int32
	viewportWidth  int32
	viewportHeight int32

	startTime   time.Time     // Application start time
	frameStart  time.Time     // Frame start time
	frameDelta  time.Duration // Duration of last frame
	modelMatrix mgl32.Mat4
	viewMatrix  mgl32.Mat4

	projectionMatrix mgl32.Mat4
}

func (glsm *GlStateManager) Reset() {
	glsm.startTime = time.Now()
	glsm.frameStart = time.Now()
	glsm.modelMatrix = mgl32.Ident4()
	glsm.viewMatrix = mgl32.Ident4()
	glsm.projectionMatrix = mgl32.Ident4()
	glsm.Window.SetSizeCallback(glsm.onResize)

	width, height := glsm.Window.GetSize()
	glsm.viewportX = 0
	glsm.viewportY = 0
	glsm.viewportWidth = int32(width)
	glsm.viewportHeight = int32(height)

	glsm.projectionMatrix = mgl32.Perspective(mgl32.DegToRad(float32(60)),
		float32(width)/float32(height),
		0.1,
		100.0)

	gl.Viewport(0, 0, int32(width), int32(height))

	if len(glsm.programs) > 0 {
		for _, prog := range glsm.programs {
			prog.Delete()
		}
	}
	glsm.programs = make(map[string]*Program)
	glsm.prepareFrame()
}

func (glsm *GlStateManager) prepareFrame() {
	now := time.Now()
	glsm.frameDelta = now.Sub(glsm.frameStart)
	glsm.frameStart = now

	gl.Clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT)
	gl.ClearColor(1, 1, 1, 0.0)
	gl.PolygonMode(gl.FRONT_AND_BACK, gl.FILL)
	//gl.Enable(gl.BLEND)
	//gl.Enable(gl.DEBUG_OUTPUT)
	//gl.Enable(gl.MULTISAMPLE)
}

func (glsm *GlStateManager) useProgram(name string) {
	glsm.programs[name].Use()
	glsm.prog = glsm.programs[name]
}

func (glsm *GlStateManager) onResize(w *glfw.Window, width, height int) {
	glsm.viewportX = 0
	glsm.viewportY = 0
	glsm.viewportWidth = int32(width)
	glsm.viewportHeight = int32(height)

	glsm.projectionMatrix = mgl32.Perspective(mgl32.DegToRad(float32(60)),
		float32(width)/float32(height),
		0.1,
		100.0)

	gl.Viewport(0, 0, int32(width), int32(height))
}

func (glsm *GlStateManager) updateTimer() {
	now := time.Now()
	glsm.frameDelta = now.Sub(glsm.frameStart)
	glsm.frameStart = now
}

func getGlError(glHandle uint32, checkTrueParam uint32, getObjIvFn getObjIv,
	getObjInfoLogFn getObjInfoLog, failMsg string) error {

	var success int32
	getObjIvFn(glHandle, checkTrueParam, &success)

	if success == gl.FALSE {
		var logLength int32
		getObjIvFn(glHandle, gl.INFO_LOG_LENGTH, &logLength)

		log := gl.Str(strings.Repeat("\x00", int(logLength)))
		getObjInfoLogFn(glHandle, logLength, nil, log)

		return fmt.Errorf("%s: %s", failMsg, gl.GoStr(log))
	}

	return nil
}
