package main

import (
	"camar/app"
	"runtime"
)

func main() {
	runtime.LockOSThread()
	//rtsp.GetData()
	//app.App()
	app := app.App()
	app.Run()
}
