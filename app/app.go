package app

import (
	"camar/gui"
	"camar/render"
	"camar/window"
	"github.com/go-gl/glfw/v3.2/glfw"
	"log"
)

// Application singleton
var a *Application

// Desktop application defaults
const (
	title  = "CamAr Application"
	width  = 1280
	height = 539

	SCENE_AR      = 0
	SCENE_DUOTONE = 1
	SCENE_MOSAIC  = 2
	SCENE_TWIST   = 3
	SCENE_CAT     = 4
	SCENE_AVG     = 5
	SCENE_SPLIT   = 6
	SCENE_TOON    = 7
	SCENE_SOBEL   = 8
	SCENE_FREY    = 9
)

// Application
type Application struct {
	Window *window.Win
	Scenes []*render.Scene
	Scene  *render.Scene
}

// App returns the Application singleton, creating it the first time.
func App() *Application {
	if a != nil {
		return a
	}
	app := new(Application)

	app.Window = &window.Win{
		Window:       window.Init(width, height, title),
		InputManager: window.NewInputManager(),
	}
	app.Window.SetCallbacks()

	glsm := &render.GlStateManager{
		Window: app.Window,
	}
	//Create scenes
	app.Scenes = append(app.Scenes, &render.Scene{
		Name:            "Default",
		GlStateManager:  glsm,
		LoadModel:       true,
		TextureName:     "chromo",
		BasicShaderPath: "",
	})
	app.Scenes = append(app.Scenes, &render.Scene{
		Name:            "Duotone",
		GlStateManager:  glsm,
		LoadModel:       false,
		TextureName:     "chromo",
		BasicShaderPath: "duotone/",
	})
	app.Scenes = append(app.Scenes, &render.Scene{
		Name:            "Mosaic",
		GlStateManager:  glsm,
		LoadModel:       false,
		TextureName:     "chromo",
		BasicShaderPath: "mosaic/",
	})

	/*app.Scenes = append(app.Scenes, &render.Scene{
		Name: "Tornade",
		GlStateManager: glsm,
		LoadModel: false,
		TextureName: "chromo",
		BasicShaderPath: "tornade/",
	})*/
	app.Scenes = append(app.Scenes, &render.Scene{
		Name:            "Twist",
		GlStateManager:  glsm,
		LoadModel:       false,
		TextureName:     "chromo",
		BasicShaderPath: "twist/",
	})
	app.Scenes = append(app.Scenes, &render.Scene{
		Name:            "Cat",
		GlStateManager:  glsm,
		LoadModel:       false,
		TextureName:     "chromo",
		BasicShaderPath: "cat/",
	})
	app.Scenes = append(app.Scenes, &render.Scene{
		Name:            "Average",
		GlStateManager:  glsm,
		LoadModel:       false,
		TextureName:     "chromo",
		BasicShaderPath: "avg/",
	})
	app.Scenes = append(app.Scenes, &render.Scene{
		Name:            "Split",
		GlStateManager:  glsm,
		LoadModel:       false,
		TextureName:     "chromo",
		BasicShaderPath: "split/",
	})
	/*app.Scenes = append(app.Scenes, &render.Scene{
		Name: "Compose",
		GlStateManager: glsm,
		LoadModel: false,
		TextureName: "chromo",
		BasicShaderPath: "vr-compose/",
	})*/
	app.Scenes = append(app.Scenes, &render.Scene{
		Name:            "Toon",
		GlStateManager:  glsm,
		LoadModel:       false,
		TextureName:     "chromo",
		BasicShaderPath: "toon/",
	})
	app.Scenes = append(app.Scenes, &render.Scene{
		Name:            "Sobel",
		GlStateManager:  glsm,
		LoadModel:       false,
		TextureName:     "chromo",
		BasicShaderPath: "sobel/",
	})
	app.Scenes = append(app.Scenes, &render.Scene{
		Name:            "Frei-Chen",
		GlStateManager:  glsm,
		LoadModel:       false,
		TextureName:     "chromo",
		BasicShaderPath: "outline-Frei-Chen/",
	})

	/*app.Scenes = append(app.Scenes, &render.Scene{
		Name: "Denoise",
		GlStateManager: glsm,
		LoadModel: false,
		TextureName: "noise",
		BasicShaderPath: "denoise/",
	})*/

	app.Scene = app.Scenes[1]
	a = app
	return a
}

func (app *Application) Run() {
	gui.Init(app.Window)
	app.Scene.Load(app.Window.InputManager)

	for !app.Window.ShouldClose() {
		app.Window.InputManager.CheckpointCursorChange()
		app.Scene.Update()

		//sceneIdx := gui.Render(app.Scenes)
		//sceneIdx := 13
		if app.Window.InputManager.IsActive(window.Scene0) {
			log.Println("[INFO] Scene change")
			app.Scene = app.Scenes[SCENE_AR]
			app.Scene.Load(app.Window.InputManager)
		}
		if app.Window.InputManager.IsActive(window.Scene1) {
			log.Println("[INFO] Scene change")
			app.Scene = app.Scenes[SCENE_DUOTONE]
			app.Scene.Load(app.Window.InputManager)
		}
		if app.Window.InputManager.IsActive(window.Scene2) {
			log.Println("[INFO] Scene change")
			app.Scene = app.Scenes[SCENE_MOSAIC]
			app.Scene.Load(app.Window.InputManager)
		}
		if app.Window.InputManager.IsActive(window.Scene3) {
			log.Println("[INFO] Scene change")
			app.Scene = app.Scenes[SCENE_TWIST]
			app.Scene.Load(app.Window.InputManager)
		}
		if app.Window.InputManager.IsActive(window.Scene4) {
			log.Println("[INFO] Scene change")
			app.Scene = app.Scenes[SCENE_CAT]
			app.Scene.Load(app.Window.InputManager)
		}
		if app.Window.InputManager.IsActive(window.Scene5) {
			log.Println("[INFO] Scene change")
			app.Scene = app.Scenes[SCENE_AVG]
			app.Scene.Load(app.Window.InputManager)
		}
		if app.Window.InputManager.IsActive(window.Scene6) {
			log.Println("[INFO] Scene change")
			app.Scene = app.Scenes[SCENE_SPLIT]
			app.Scene.Load(app.Window.InputManager)
		}
		if app.Window.InputManager.IsActive(window.Scene7) {
			log.Println("[INFO] Scene change")
			app.Scene = app.Scenes[SCENE_TOON]
			app.Scene.Load(app.Window.InputManager)
		}
		if app.Window.InputManager.IsActive(window.Scene8) {
			log.Println("[INFO] Scene change")
			app.Scene = app.Scenes[SCENE_SOBEL]
			app.Scene.Load(app.Window.InputManager)
		}
		if app.Window.InputManager.IsActive(window.Scene9) {
			log.Println("[INFO] Scene change")
			app.Scene = app.Scenes[SCENE_FREY]
			app.Scene.Load(app.Window.InputManager)
		}
		// Swap buffers and poll events
		app.Window.SwapBuffers()
		glfw.PollEvents()
	}

}
