package gui

import "C"
import (
	"camar/render"
	"camar/window"
	"github.com/go-gl/glfw/v3.2/glfw"
	"github.com/golang-ui/nuklear/nk"
	"log"
)

var ctx *nk.Context
var win *glfw.Window
var sceneIdx = 0

func Init(window *window.Win) {
	window.MakeContextCurrent()
	ctx = nk.NkPlatformInit(window.Window, nk.PlatformDefault)
	win = window.Window
	atlas := nk.NewFontAtlas()
	nk.NkFontStashBegin(&atlas)
	nk.NkFontStashEnd()
}

func Render(scenes []*render.Scene) int {
	nk.NkPlatformNewFrame()
	width, height := win.GetSize()
	bounds := nk.NkRect(float32(width-5-230), 5, 230, float32(height-10))
	update := nk.NkBegin(ctx, "Scenes", bounds, nk.WindowBorder|nk.WindowMinimizable|nk.WindowTitle)
	var color nk.Color

	color.SetRGBAi(20, 20, 20, 140)
	nk.SetBackgroundColor(ctx, color)

	if update > 0 {
		nk.NkLayoutRowStatic(ctx, 45, 200, 1)
		{
			for i, scene := range scenes {
				if nk.NkButtonLabel(ctx, scene.Name) > 0 {
					log.Println("[INFO] button pressed!")
					sceneIdx = i
				}
			}
		}

	}
	nk.NkEnd(ctx)
	nk.NkPlatformRender(nk.AntiAliasingOff, 512*1024, 512*1024)
	return sceneIdx
}
