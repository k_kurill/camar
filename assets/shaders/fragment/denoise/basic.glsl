#version 410
uniform sampler2D tInput;
uniform float exponent = 40.0;
uniform float strength = 2.0;
uniform vec2 resolution = vec2 (300.0, 300.0);
varying vec2 TexCoord;

void main() {

    vec4 center = texture2D(tInput, TexCoord);
    vec4 color = vec4(0.0);
    float total = 0.0;
    for (float x = -4.0; x <= 4.0; x += 1.0) {
        for (float y = -4.0; y <= 4.0; y += 1.0) {
            vec4 sampler = texture2D(tInput, TexCoord + vec2(x, y) / resolution);
            float weight = 1.0 - abs(dot(sampler.rgb - center.rgb, vec3(0.25)));
            weight = pow(weight, exponent);
            color += sampler * weight;
            total += weight;
        }
    }
    gl_FragColor = color / total;
}