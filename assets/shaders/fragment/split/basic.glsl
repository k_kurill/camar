#version 410

varying vec2 TexCoord;
uniform sampler2D tInput;
uniform vec2 delta = vec2 (30.0, 20.0);
uniform vec2 resolution = vec2 (1290.0, 539.0);

void main() {

    vec2 dir = TexCoord - vec2( .5 );
    float d = .7 * length( dir );
    normalize( dir );
    vec2 value = d * dir * delta;

    vec4 c1 = texture2D( tInput, TexCoord - value / resolution.x );
    vec4 c2 = texture2D( tInput, TexCoord );
    vec4 c3 = texture2D( tInput, TexCoord + value / resolution.y );

    gl_FragColor = vec4( c1.r, c2.g, c3.b, c1.a + c2.a + c3.b );

}