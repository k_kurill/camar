#version 410
out vec4 frag_colour;
in vec2 TexCoord;

uniform sampler2D ourTexture0;

void main() {
    frag_colour = texture(ourTexture0, TexCoord);
}