#version 410
varying vec2 TexCoord;
uniform sampler2D texture;

uniform float u_time;
uniform vec2 u_resolution = vec2 (1290.0, 539.0);
uniform float u_rand;
uniform float u_noise;
uniform vec2 u_mouse;

uniform int nrows = 30;

void main() {

    int ncols = nrows * 2;
    vec2 p = TexCoord.st;

    vec2 pd = vec2(floor(p.x * ncols) / ncols, floor(p.y * nrows) / nrows);

    vec4 col = texture2D(texture, pd);

    gl_FragColor = col;
}