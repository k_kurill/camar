#version 410
#define PI 3.14159265358
#define TWO_PI 6.28318530718

varying vec2 TexCoord;
uniform sampler2D texture;

uniform vec2 u_mouse;
uniform float u_time;
uniform vec2 u_resolution = vec2 (1290.0, 539.0);
uniform float u_rand;
uniform float u_noise;

void main() {

    vec2 p = TexCoord.st;

    float c = pow(abs(sin(u_time*1.7 + u_rand)), 25);
    float x = 0.7 * (u_noise*u_noise+u_noise - 0.5) * cos(5.0*p.y + u_noise + u_time);
    float y = 1.2 * (u_noise - 0.5) * sin(10.0*p.x + u_time);
    vec2 q = vec2(x, y);
    vec4 col = texture2D(texture, p + c * q);

    gl_FragColor = col;
}

