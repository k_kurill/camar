#version 410

uniform sampler2D tInput;
uniform vec2 resolution = vec2 (1290.0, 539.0);
uniform sampler2D leftEyeTexture;
uniform sampler2D rightEyeTexture;

varying vec2 TexCoord;


void main(void) {

    if( TexCoord.x < .5 ) {
        gl_FragColor = texture2D( leftEyeTexture, TexCoord * vec2( 2., 1. ) );
    } else {
        gl_FragColor = texture2D( rightEyeTexture, TexCoord * vec2( 2., 1. ) - vec2( 1., 0. ) );
    }

}